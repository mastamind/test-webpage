<?php
include_once 'cd.class.php'; // include all the necessary class files
include_once 'furniture.class.php';
include_once 'book.class.php';
include_once 'db.class.php';
$db = new Database();
$names = ['name', 'sku', 'price', 'typeswitcher']; //name array for checking
$error = false;  //defines an error variable to know if any fields are empty
// Server side validation, technically not needed since JS handles that on the client side, but it is good practise to do server side validation as well
if ('POST' == $_SERVER['REQUEST_METHOD']) { // checks if fields are left empty, if not increments the $error var
    for ($i = 0; $i < sizeof($names); ++$i) {
        if (empty($_POST[$names[$i]])) {
            $error = true;
        }
    } //checks if the fields value isnt empty
    switch ($_POST['typeswitcher']) {
            case 'furniture':
                $obj = new Furniture($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['height'], $_POST['width'], $_POST['length']);
                break;
            case 'cd':
                $obj = new CD($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['size']);
                break;
            case 'book':
                $obj = new Book($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['weight']);
                break;
        }
    //checks whether there was an error or not, then executes the statement
    if (false == $error) {
        $db->statement('INSERT INTO products(sku, name, price, type, attribute) values(?,?,?,?,?)', [$obj->getsku(), $obj->getname(), $obj->getprice(), $obj->get_type(), $obj->getattribute()]);
    }
    header('Location: index.php'); //redirects back to the original page
}
