<!DOCTYPE HTML>
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<title>Products</title>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="add.js" type="text/javascript"></script>
</head>

<body>
	<div class="container-fluid">
		<!--creates the top form with the select element for choosing to either add a product or delete one or more products, and a button -->
		<form id="topform">
			<!-- creates an inline form, perhaps some other layout option would better depending on the design, but that could be pretty easily changed-->
			<div class="form-inline">
				<h1 class="display-2">Products</h1>
				<!-- creates the select element(dropdown) with 2 options at 25% of device-width -->
				<select class="w-25 form-control ml-5" id="sel1">
					<option value="add">Add</option>
					<option value="md">Mass delete</option>
				</select>
				<!--creates a button that when clicked will exectute a JS function that is explained in more detail below-->
				<button type="button" onclick="checkdrop()" class="btn ml-2 indexpagebtn">Apply</button>
				<!--btn btn-secondary-->
			</div>
		</form>
		<hr>
		<!-- the div that houses the cards -->
		<div class="container-fluid mt-4">
			<div class="row justify-content-left" name="cardeck">
				<!-- this div and the one above it can be made into one and that would probably be best -->
				<?php
                include_once 'db.class.php';
                $db=new Database();
                $products=$db->list_products();
                for ($i=0;$i<sizeof($products);$i++) { // while to show all of the cards fetch method is associative array, but object could also be used if wanted
                    ?>
					<!-- the code that creates an individual card with the appropriate styling options-->
					<div class="col-auto mb-3">
						<div class="card <?php echo $products[$i]->getAppropriateTypeName(); ?>" style="width: 22.75vw;">
							<div class="row no-gutters">
								<div class="pl-2 pt-1">
									<div class="form-group form-check">
										<!-- creates a checkbox that has a unique id based on the ID field from the DB table. On click this will execute a JS script that add/remove the checkbox id to an array. For more details see below -->
										<input type="checkbox" class="form-check-input" name="checkboxcard" onclick="markcheckbox(this.id)" id=<?php echo "cbox" . $products[$i]->getid(); ?>>
									</div>
								</div>
								<div class="col-md-11">
									<div class="card-body">
										<!-- echos the correct values from the associative array -->
										<p class="card-text text-center"><?php echo $products[$i]->getsku(); ?></p>
										<p class="card-text text-center"><?php echo $products[$i]->getname(); ?></p>
										<p class="card-text text-center"><?php echo $products[$i]->getprice(); ?>$</p>
										<!-- checks the type and echoes the appropriate word as well as the attribute -->
										<p class="card-text text-center"><?php echo $products[$i]->getAppropriateTypeDesc(); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>

				
		<?php	}?>
			
			
			</div>
		</div>
</body>

</html>