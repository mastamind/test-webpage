<?php
include_once 'db.class.php';
class Product // the base class
{
    //variable that are needed to be accessed by all child classes
    protected $SKU = '';
    protected $NAME = '';
    protected $PRICE = 0;
    protected $TYPE = '';
    protected $ATTRIBUTE; //define class specific var
    protected $ID;

    public function __construct($id, $sku, $name, $price, $type, $attribute) // the main constructur, defines variables that are common across all product types
    {
        $this->ATTRIBUTE = $attribute;
        $this->TYPE = $type;
        $this->ID = $id;
        $this->assigncommon($sku, $name, $price);
    }

    public function assigncommon($sku, $name, $price)
    {
        $this->SKU = $sku;
        $this->NAME = $name;
        $this->PRICE = $price;
    }

    //functions to access class variables since they are protected
    public function getsku()
    {
        return $this->SKU;
    }

    public function getname()
    {
        return $this->NAME;
    }

    public function getprice()
    {
        return $this->PRICE;
    }

    public function get_type()
    {
        return $this->TYPE;
    }

    public function getid()
    {
        return $this->ID;
    }

    public function getattribute()
    {
        return $this->ATTRIBUTE;
    }

    public function getAppropriateTypeName()
    {
        switch ($this->TYPE) {
            case 'furniture':
                return 'furniturecard';
                break;
            case 'cd':
                return 'cdcard';
                break;
            case 'book':
                return 'bookcard';
                break;
        }
    }

    public function getAppropriateTypeDesc()
    {
        switch ($this->TYPE) {
            case 'furniture':
                return 'Dimensions: '.$this->ATTRIBUTE;
                break;
            case 'cd':
                return 'Weight: '.$this->ATTRIBUTE.' KG';
                break;
            case 'book':
                return 'Weight: '.$this->ATTRIBUTE.' KG';
                break;
        }
    }
}
