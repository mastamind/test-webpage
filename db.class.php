<?php
include_once 'product.class.php';
class Database
{
	//define variables
    private $conn;
    private $servername = 'localhost';
    private $username = 'root';
    private $password = 'root';
    private $dbname = 'scandiweb';
    protected $stmt;
    protected $selectAllstring = 'SELECT id, sku, name, price, type, attribute FROM products';

    public function __construct() //when class is constructed, initiate connection
    {
        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Error: '.$e->getMessage();
        }
    }

    public function statement($statement, $params = '', $defaultFetchmode = PDO::FETCH_ASSOC) //executes the passed statement with the passed (if aplicable) params
    {
        $this->stmt = $this->conn->prepare($statement);
        $this->stmt->execute($params);

        $SQLopt = strtolower(explode(' ', $statement)[0]); //gets the statement function e.g. SELECT, DELETE, INSERT
        if ('select' == $SQLopt) {
            return $this->stmt->fetchAll($defaultFetchmode); //returns the result if the function is SELECT
        } elseif ('delete' == $SQLopt || 'insert' == $SQLopt) {
            return $this->stmt->rowCount(); //returns affected rows
        }
    }

    public function list_products() //lists all products
    {
        $products = array();
        $this->stmt = $this->conn->query($this->selectAllstring);
        while ($row = $this->stmt->fetch(PDO::FETCH_ASSOC)) {
            $product = new Product($row['id'], $row['sku'], $row['name'], $row['price'], $row['type'], $row['attribute']); //creates a new product
            $products[] = $product;
        }

        return $products; //returns an indexable array of product objects
    }

    public function __destruct() //close the connection when class is destructed
    {
        $this->conn = null;
    }
}
