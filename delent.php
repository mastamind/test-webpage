<?php
include_once 'db.class.php';
$db = new Database();
$delete = $_REQUEST['d']; //Get the string from AJAX with the identifier 'd'
$deletearray=explode(',', $delete); //convert $delete string to array so that it'll be easier to work with
//make a for loop to get the ids from the array to the string
for ($i=0;$i<sizeof($deletearray);$i++) {
    $db->statement("DELETE FROM Products where id=?", [str_replace("cbox", "", $deletearray[$i])]);
}
// executes the prepared statement with the corresponding element from the array trimming the "cbox" indentifier part out of the string
