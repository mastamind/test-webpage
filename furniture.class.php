<?php
include_once 'product.class.php';
class Furniture extends Product // create a child class Furniture that is a product
{
    //define class specific vars
    protected $HEIGHT = 0;
    protected $WIDTH = 0;
    protected $LENGTH = 0;

    public function __construct($sku, $name, $price, $height, $width, $length)//class specific constructur
    {
        //assign varibles
        $this->assigncommon($sku, $name, $price);
        $this->HEIGHT = $height;
        $this->WIDTH = $width;
        $this->LENGTH = $length;
        $this->TYPE = 'furniture';
    }

    public function getattribute()// class specific function to get the unique attribute
    {
        return $this->LENGTH.'x'.$this->WIDTH.'x'.$this->HEIGHT; //concats the attribute, so that it takes up only one DB field
    }
}
