//function that decides which fields need to be added/removed
		function alterform(val) {
			switch (val) {
				case "furniture": //if type furniture is selected
					addfield("dimension");
					removefield("size");
					removefield("weight");
					break;
				case "cd": //if type CD is selected
					addfield("size");
					removefield("weight");
					removefield("dimension");
					break;
				case "book": //if type book is selected
					addfield("weight");
					removefield("size");
					removefield("dimension");
					break;
			}
		}
		//function adds a field when called
		function addfield(name) {
			var addwhere = document.getElementById('variablefields'); //gets the element in js so that the DOM can be manipulated
			var newelement = document.createElement("div"); //creates a new class
			newelement.setAttribute("class", name + "class"); //sets an attribute so that js can delete the element if necessary
			switch (name) //sets the innerHTML of the new element according to the selected type
			{
				case "dimension":
					newelement.innerHTML = '<div class="form-group row"><label for="Height" class="col-sm-2 col-form-label">Height</label><input type="number" step="0.01" class="w-25 form-control" id="height" name="height" placeholder="Height" data-toggle="tooltip"  title="Height should be in mm"></div> \
		<div class="form-group row"><label for="Length" class="col-sm-2 col-form-label">Length</label><input type="number" step="0.01" class="w-25 form-control" id="length" name="length" placeholder="Length" data-toggle="tooltip"  title="Length should be in mm"></div> \
		<div class="form-group row"><label for="Width" class="col-sm-2 col-form-label">Width</label><input type="number" step="0.01" class="w-25 form-control" id="width" name="width" placeholder="Width" data-toggle="tooltip"  title="Width should be in mm"></div>';
					break;
				case "weight":
					newelement.innerHTML = '<div class="form-group row"><label for="weight" class="col-sm-2 col-form-label">Weight</label><input type="number" step="0.01" class="w-25 form-control" id="weight" name="weight" placeholder="Weight" data-toggle="tooltip" title="Weight should be a numeric value in KG"></div>';
					break;
				case "size":
					newelement.innerHTML = '<div class="form-group row"><label for="size" class="col-sm-2 col-form-label">Size</label><input type="number" step="0.01" class="w-25 form-control" id="size" name="size" placeholder="Size" data-toggle="tooltip"  title="Size should be a numeric value in MB"></div>';
					break;
			}
			addwhere.appendChild(newelement); //appends the newelement to the already existing class as a child
		}
		//function removes the desired field
		function removefield(name) {
			$('.' + name + "class").remove();
		}
		//define an error var to know if any field is empty
		var error = 0;
		//function checks if something is entered into fields
		function checkfields() {
			//reset the error to 0
			error = 0;
			//gets all of the fields that have the tag <input> and populate them into an array
			var allfields = document.getElementsByTagName("input");
			//for loop cycles thru the array and checks if every individual input element's value isnt null
			for (var i = 0; i < allfields.length; i++)
				if (allfields[i].value == "") setwarning(allfields[i].id);
			//since the dropdown isnt an input field the for loop doesnt check it
			if (document.getElementById("typeswitcher").value == "") setwarning("typeswitcher");
			//if no errors are found, the form gets submitted
			if (error == 0) document.getElementById("inputs").submit();
		}
		//function adds a bootstrap class "bg-warning" which basically colors the field orange to let the user know if the field's value is null and incriments the error var to prevent the form being submitted with an empty field
		function setwarning(el) {
			document.getElementById(el).classList.add("bg-warning");
			document.getElementById(el).setAttribute("oninput", "removewarning(this.id)"); //sets an attribute whenever there's an input to that field to remove the error
			error++;
		}
		//function removes a class to the desired field
		function removewarning(el) {
			document.getElementById(el).classList.remove("bg-warning"); //removes that particular class
		}
		
//define a var array that'll house which checkboxes are checked
			var ids = [];
			//function checks which value is selected in the dropdown (add or mass delete)
			function checkdrop() {
				var e = document.getElementById("sel1").value; // the value selected in the dropdown
				//checks which value is selected
				if (e == "add") {
					document.getElementById("topform").action = "add.php"; //sets the action when form is submitted (this could also be achieved with a simple redirect since no data needs to be transferred)
					document.getElementById("topform").submit(); //submits the form
				} else if (e == "md" && ids.length > 0) { //checks if mass delete is selected as well as if any checkboxes are checked so that a blank ajax isnt sent
					//As a side note, i know there are probably better ways to handle the deletion process, but i chose this method because i wanted to experiment with JS
					var xhttp = new XMLHttpRequest(); // initiates a new XMLHttpRequest 
					xhttp.onreadystatechange = function() { // sets the function when the request is completed 
						if (this.readyState == 4 && this.status == 200) {
							document.location.reload(true); // in this case it will reload the page, so that entries will appear deleted, but this could also be achieved by JS removing classes, like in the add.php page
						}
					};
					xhttp.open("POST", "delent.php?d=" + ids.join(","), true); // opens the XMLHttpRequest to a delent.php file and converting the array to a string with a ',' as seperator
					xhttp.send(); //sends the request
				}

			}
			//function adds or removes the id of the checkbox to the array
			function markcheckbox(primarykey) {
				if (!ids.includes(primarykey)) //firstly it checks whether this id hasn't been added already
					ids.push(primarykey); //if it hasn't been added, it adds it to the array
				else {
					var index = ids.indexOf(primarykey); // if it has been added, then it has to be removed, so this gets the 
					ids.splice(index, 1); // removes the element from the array with the corresponding index
				}
			}		
		
		