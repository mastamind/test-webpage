<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!--sets the title -->
	<title>Product Add</title>
</head>

<body>
	<!--creates a container-float with padding from topside and left side -->
	<div class="container-float pt-3 pl-5">
		<!--sets the from submit action to self method eliminating the possibility of script adding by using function htmlspecialchars -->
		<form id="inputs" action="insertent.php" method="POST">
			<h1 class="display-2">Product add</h1>
			<hr>
			<!-- creates a form-group row class for the SKU field -->
			<div class="form-group row">
				<!--creates a label with padding/spacing using bootstrap's grid system -->
				<label for="sku" class="col-sm-2 col-form-label">SKU</label>
				<!--creates an input field for the SKU with the use of boostrap's grid system -->
				<input type="text" class="w-25 form-control" id="sku" name="sku" placeholder="Enter SKU">
			</div>
			<!--Repeats the same for the name field, for additional explanation see above-->
			<div class="form-group row">
				<label for="name" class="col-sm-2 col-form-label">Name</label>
				<input type="text" class="w-25 form-control" id="name" name="name" placeholder="Name">
			</div>
			<!--Repeats the same for the price field, for additional explanation see above-->
			<div class="form-group row">
				<label for="price" class="col-sm-2 col-form-label">Price</label>
				<input type="number" class="w-25 form-control" id="price" name="price" step="0.01" placeholder="Price">
			</div>
			<!-- creates a form-group row class for a dropdown element -->
			<div class="form-group row">
				<label for="typeswitcher" class="col-sm-2 col-form-label">Type Switcher</label>
				<!--creates a select element(dropdown) with options containing the various product types and on the onchange executes a JS function adding the necessary input fields-->
				<select id="typeswitcher" name="typeswitcher" class="w-25 form-control" onchange="alterform(this.value)">
					<!-- sets the default selected value to let the user know to change the type (alternatively this could be done differently i guess) -->
					<option selected value="">Choose type</option> <!-- sets the default selected value to let the user know to change the type (alternatively this could be done differently i guess) -->
					<option value="furniture">Furniture</option>
					<option value="cd">CD</option>
					<option value="book">Book</option>
				</select>
			</div>
			<!--creates a div which will cointain the variable fields based on product type -->
			<div id="variablefields">
			</div>
			<!--creates a button that when pressed will validate the fields with a JS function that is explained more detailed below -->
			<button type="button" class="btn btn-primary text-right" onclick="checkfields()">Insert</button>
		</form>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="add.js" type="text/javascript"></script>
</body>

</html>