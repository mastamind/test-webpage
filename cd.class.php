<?php
include_once 'product.class.php';
class CD extends Product // create a child class CD that is a product
{
    protected $SIZE = 0; //define class specific var

    public function __construct($sku, $name, $price, $size) //class specific constructur
    {
        //assign variables
        $this->assigncommon($sku, $name, $price);
        $this->SIZE = $size;
        $this->TYPE = 'cd';
    }

    public function getattribute()// class specific function to get the unique attribute
    {
        return $this->SIZE;
    }
}
