<?php
include_once 'product.class.php';
class Book extends Product // create a child class book that is a product
{
    protected $WEIGHT = 0; //define class specific var

    public function __construct($sku, $name, $price, $weight) //class specific constructur
    {
        //assign varibles
        $this->WEIGHT = $weight;
        $this->TYPE = 'book';
        $this->assigncommon($sku, $name, $price);
    }

    public function getattribute() // class specific function to get the unique attribute
    {
        return $this->WEIGHT;
    }
}
